package ua.lokha.oneshot;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Events implements Listener {

    @EventHandler
    public void on(PlayerQuitEvent event) {
        Arena arena = OneShotPlugin.getInstance().getArenaByPlayer(event.getPlayer());
        if (arena != null) {
            arena.quit(event.getPlayer());
        }

        Metadata.remove(event.getPlayer());
    }

    @EventHandler
    public void on(PlayerDeathEvent event) {
        Arena arena = OneShotPlugin.getInstance().getArenaByPlayer(event.getEntity());
        if (arena != null && arena.getStage().equals(Stage.GAME)) {
            arena.kill(event.getEntity(), null);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void on(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player player = (Player) event.getEntity();
            if (player.getHealth() - event.getFinalDamage() <= 0) {
                Player damager = (Player) event.getDamager();

                Arena arena = OneShotPlugin.getInstance().getArenaByPlayer(player);
                Arena damagerArena = OneShotPlugin.getInstance().getArenaByPlayer(damager);

                if (arena != null && arena.equals(damagerArena)) {
                    arena.kill(player, damager);
                }
            }
        }
    }
}
