package ua.lokha.oneshot;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public class CommandJoinArena implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Можно писать только игроку.");
            return false;
        }

        Player player = (Player) sender;

        List<Arena> arenas = OneShotPlugin.getInstance().getArenas();

        if (args.length == 0){
            sender.sendMessage("§cУкажите имя арены, в которую хотите войти, список арен: " +
                    arenas.stream().map(Arena::getName).collect(Collectors.joining(", ")));
            return false;
        }

        String arenaName = args[0];
        Arena findArena = OneShotPlugin.getInstance().getArenaByName(arenaName);

        if (findArena == null) {
            sender.sendMessage("§cИмя арены не найдено, список арен: " +
                    arenas.stream().map(Arena::getName).collect(Collectors.joining(", ")));
            return false;
        }

        if (!findArena.getStage().equals(Stage.LOBBY)) {
            sender.sendMessage("§cЭта арена уже запущена, дождитесь окончания игры.");
            return false;
        }

        if (OneShotPlugin.getInstance().getArenaByPlayer(player) != null) {
            sender.sendMessage("§cВы уже играете в арене.");
            return false;
        }


        findArena.join(player);
        return true;
    }
}
