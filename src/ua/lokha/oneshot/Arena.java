package ua.lokha.oneshot;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

import java.util.*;

public class Arena {

    private String name;
    private Stage stage = Stage.LOBBY;
    private Location lobbyPos;
    private List<Location> spawnPos;

    private List<Player> players = new ArrayList<>();
    private Map<Player, ArenaPlayer> playerData = new HashMap<>();
    private BukkitTask timer;
    private int secondsToStart;

    public Arena(String name, Location lobbyPos, List<Location> spawnPos) {
        this.name = name;
        this.lobbyPos = lobbyPos;
        this.spawnPos = spawnPos;
    }

    public List<Location> getSpawnPos() {
        return spawnPos;
    }

    public String getName() {
        return name;
    }

    public Location getLobbyPos() {
        return lobbyPos;
    }

    public Stage getStage() {
        return stage;
    }

    public void join(Player player) {
        this.players.add(player);

        this.broadcast("§aИгрок " + player.getName() + " зашел на арену " + players.size() + "/" + OneShotPlugin.getInstance().getMinPlayers());
        player.teleport(lobbyPos);

        if (players.size() >= OneShotPlugin.getInstance().getMinPlayers()) {
            this.startTimer();
        }
    }

    public void quit(Player player) {
        this.playerClear(player);
        this.players.remove(player);

        if (stage.equals(Stage.LOBBY)) {
            this.broadcast("§aИгрок " + player.getName() + " вышел " + players.size() + "/" + OneShotPlugin.getInstance().getMinPlayers());
        } else if (stage.equals(Stage.GAME)) {
            this.broadcast("§aИгрок " + player.getName() + " вышел, осталось в живых " + players.size());
        }

        if (this.players.size() <= 1) {
            if (stage.equals(Stage.LOBBY) && players.size() < OneShotPlugin.getInstance().getMinPlayers()) {
                this.stopTimer();
            } else if (stage.equals(Stage.GAME)) {
                if (players.size() <= 1) {
                    this.stopGame();
                }
            }
        }
    }

    public void kill(Player player, Player killer) {
        this.playerClear(player);
        this.players.remove(player);

        if (killer != null) {
            this.broadcast("§aИгрок " + player.getName() + " убит " + killer.getName() + ", осталось в живых " + players.size());
            player.sendMessage("§cВас убил " + killer.getName());

            playerData.get(killer).addKill();
        } else {
            this.broadcast("§aИгрок " + player.getName() + " умер, осталось в живых " + players.size());
            player.sendMessage("§cВы погибли.");
        }

        player.teleport(lobbyPos);

        if (stage.equals(Stage.GAME)) {
            if (players.size() <= 1) {
                this.stopGame();
            }
        }
    }

    private void playerClear(Player player) {
        if (stage.equals(Stage.GAME)) {
            playerData.get(player).playerClear();
        }
    }

    private void stopGame() {
        List<ArenaPlayer> players = new ArrayList<>(playerData.values());
        players.sort((o1, o2) -> o2.getKills() - o1.getKills());

        StringBuilder builder = new StringBuilder();
        builder.append("§eИгра окончена, победители:");
        for (ArenaPlayer player : players) {
            if (player.getKills() == 0) {
                break;
            }
            builder.append("\n§a- ").append(player.getPlayer().getName()).append(" - ").append(player.getKills());
        }
        for (Player player : playerData.keySet()) {
            if (player.isOnline()) {
                player.sendMessage(builder.toString());
            }
        }

        stage = Stage.LOBBY;
        this.players.clear();
        playerData.clear();
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void startTimer() {
        if (timer != null) {
            return;
        }
        secondsToStart = OneShotPlugin.getInstance().getSecondsToStart();
        timer = Bukkit.getScheduler().runTaskTimer(OneShotPlugin.getInstance(),
                () -> {
                    if (secondsToStart <= 0) {
                        timer.cancel();
                        timer = null;
                        this.startGame();
                    } else {
                        this.broadcast("§eДо начала игры " + secondsToStart + " секунд.");
                        secondsToStart--;
                    }
                }, 0, 20);
    }

    private void startGame() {
        stage = Stage.GAME;

        this.broadcast("§eИгра началась!");

        for (Player player : players) {
            playerData.put(player, new ArenaPlayer(player));
        }

        List<Location> randomPos = new ArrayList<>(spawnPos);
        Collections.shuffle(randomPos);

        for (Player player : players) {
            if (randomPos.isEmpty()) {
                randomPos = new ArrayList<>(spawnPos);
                Collections.shuffle(randomPos);
            }
            Location pos = randomPos.remove(0);
            player.teleport(pos);
        }
    }


    public void broadcast(String message) {
        for (Player player : players) {
            player.sendMessage(message);
        }
    }

    public Collection<Player> getPlayers() {
        return players;
    }
}
