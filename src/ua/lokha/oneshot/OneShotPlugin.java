package ua.lokha.oneshot;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class OneShotPlugin extends JavaPlugin {

    private static OneShotPlugin instance;

    public static OneShotPlugin getInstance() {
        return instance;
    }

    private int minPlayers = 2;
    private int secondsToStart = 10;
    private List<Arena> arenas = new ArrayList<>();

    public OneShotPlugin() {
        instance = this;
    }

    @Override
    public void onEnable() {
        this.getLogger().info("OneShot enabled");
        Bukkit.getPluginManager().registerEvents(new Events(), this);

        this.getCommand("arena").setExecutor(new CommandArena());
        this.getCommand("joinarena").setExecutor(new CommandJoinArena());
    }

    public Arena getArenaByPlayer(Player player) {
        for (Arena arena : arenas) {
            if (arena.getPlayers().contains(player)) {
                return arena;
            }
        }
        return null;
    }

    public Arena getArenaByName(String name) {
        for (Arena arena : arenas) {
            if (arena.getName().equalsIgnoreCase(name)) {
                return arena;
            }
        }
        return null;
    }

     public List<Arena> getArenas() {
        return arenas;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public int getSecondsToStart() {
        return secondsToStart;
    }
}
