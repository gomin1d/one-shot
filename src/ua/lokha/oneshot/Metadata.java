package ua.lokha.oneshot;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class Metadata {
    private static Map<Player, Metadata> metadataMap = new HashMap<>();

    public static Metadata get(Player player) {
        Metadata metadata = metadataMap.get(player);
        if (metadata == null) {
            metadata = new Metadata(player);
            metadataMap.put(player, metadata);
        }
        return metadata;
    }

    public static void remove(Player player) {
        metadataMap.remove(player);
    }

    /////////////////////////////////////////////////

    private Player player;
    private Arena createArena;

    public Metadata(Player player) {
        this.player = player;
    }

    public void setCreateArena(Arena createArena) {
        this.createArena = createArena;
    }

    public Arena getCreateArena() {
        return createArena;
    }
}
