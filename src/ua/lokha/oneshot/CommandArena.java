package ua.lokha.oneshot;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CommandArena implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Можно писать только игроку.");
            return false;
        }

        Player player = (Player) sender;

        if (args.length == 0 || args[0].equalsIgnoreCase("help")) {
            sender.sendMessage(
                    "§e==========[Arena]==========\n" +
                            "§4/arena create [name] §7- начать создание арены\n" +
                            "                           §7позиция игрока будет позицией лобби\n" +
                            "§4/arena addspawnpos §7- добавить точку спавна\n" +
                            "§4/arena save §7- завершить создание арены");
            return true;
        }

        if (args[0].equalsIgnoreCase("create")) {
            Metadata metadata = Metadata.get(player);
            if (metadata.getCreateArena() != null) {
                sender.sendMessage("§cВы уже создаете арену, завершите ее создание - /arena save");
                return false;
            }

            if (args.length < 2) {
                sender.sendMessage("§cУкажите имя арены /arena create [name]");
                return false;
            }

            String arenaName = args[1];
            Arena arena = OneShotPlugin.getInstance().getArenaByName(arenaName);
            if (arena != null) {
                sender.sendMessage("§cАрена с таким именем уже существует.");
                return false;
            }

            metadata.setCreateArena(new Arena(arenaName, player.getLocation(), new ArrayList<>()));
            sender.sendMessage("§aВы начали создавать арену, добавьте в нее точки спавна - /areana addspawnpos");
            return true;
        }

        if (args[0].equalsIgnoreCase("addspawnpos")) {
            Metadata metadata = Metadata.get(player);
            if (metadata.getCreateArena() == null) {
                sender.sendMessage("§cСначала начните создавать арену - /arena create");
                return false;
            }

            metadata.getCreateArena().getSpawnPos().add(player.getLocation());
            sender.sendMessage("§aЛокация добавлена, вы можете добавить еще одну локацию " +
                    "или закончить создание арены - /arena save");
            return true;
        }

        if (args[0].equalsIgnoreCase("save")) {
            Metadata metadata = Metadata.get(player);
            if (metadata.getCreateArena() == null) {
                sender.sendMessage("§cСначала начните создавать арену - /arena create");
                return false;
            }

            Arena arena = metadata.getCreateArena();
            if (arena.getSpawnPos().isEmpty()) {
                sender.sendMessage("§cДобавьте хотябы одну точку спавна - /arena addspawnpos");
                return false;
            }

            OneShotPlugin.getInstance().getArenas().add(arena);
            metadata.setCreateArena(null);
            sender.sendMessage("§aАрена создана.");
            return true;
        }

        sender.sendMessage("§cАргумент команды не найден, подробнее /arena help");
        return false;
    }
}
