package ua.lokha.oneshot;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class ArenaPlayer {
    private Player player;
    private int kills;

    public ArenaPlayer(Player player) {
        this.player = player;

        Scoreboard scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective objective = scoreboard.registerNewObjective("arena", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("Arena");
        player.setScoreboard(scoreboard);

        this.updateSideBar();
    }

    public int getKills() {
        return kills;
    }

    public Player getPlayer() {
        return player;
    }

    public void addKill() {
        kills++;
        this.updateSideBar();
    }

    private void updateSideBar() {
        Objective objective = player.getScoreboard().getObjective(DisplaySlot.SIDEBAR);
        objective.getScore("Убийства: ").setScore(kills);
    }

    public void playerClear() {
        player.setScoreboard(Bukkit.getScoreboardManager().getMainScoreboard());
    }
}
